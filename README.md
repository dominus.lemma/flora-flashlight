# Table of contents
[TOC]

# Overview
Silly, massively overpriced flashlight. Twist the end cap to change colors, press it in to change brightness. Power switch physically disconnects power so there is no quiescent current draw. The battery should provide 3-4 hrs of continuous run time.

# Parts list
|Quantity|Item|Link|
|---|---|---|
|1|Adafruit Flora|https://www.adafruit.com/product/659|
|1|4-pack NeoPixel|https://www.adafruit.com/product/1260
|1|SPDT Switch|https://www.adafruit.com/product/805
|1|Boost charger|https://www.adafruit.com/product/1944
|1|Inductive Charger|https://www.adafruit.com/product/1407
|1|3.7V 1.2Ah LiPoly|https://www.adafruit.com/product/258
|1|Female JST w/ pigtail|https://www.adafruit.com/product/261
|1|Rotary encoder|https://www.adafruit.com/product/377|

# Software assumptions
* The data pins of the rotary encoder are connected to GND, SCL (D3), and SDA (D2) of the Flora.
* The momentary switch pins of the rotary encoder are connected to GND and RX (D0) of the Flora.
* The NeoPixels are controlled by D6 of the Flora.
* Note: NeoPixels can be powered directly from VBATT on the Flora.

# Wiring hints
* Inductive charger connects to USB/GND pins of the boost charger
* Power switch connects to EN/GND pins of the boost charger
* Output of the boost charger can be either (+)/(-) or 5V/GND and should go to a USB micro cable that plugs into the Flora
* Lipo plugs into the boost charger's JST port

# Assembly hints
* The hardest part is properly installing the crossbar with the rotary encoder. This is most easily accomplished by mounting the encoder in the crossbar then inserting the crossbar into the body from the bottom. Carefully push it up the body while keeping it level. The top of the crossbar should be around the same level as the relief cutouts on the other side. Then place the end cap and rotate it while gently pushing up until the cap aligns with the rotary encoder. Once encoder is inside the end cap, push the crossbar all the way down until the end cap is as far down as it will go and the encoder button is fully depressed. Then, push the endcap all the way up as far as it will go. This should properly seat the rotary encoder. Test pressing the encap in. You should clearly hear the click of the momentary switch and the end cap should be returned by the switch spring. When the crossbar is properly seated, it should be glued to the sides of the body tube to ensure it does not slip.
* Using hot glue to tack pieces/wires in place is extremely useful and easy. Also, using hot glue to hold the magnets in place is a good semi-permanent way to attach them.
* By pulling hard enough, it is possible to remove the end cap. The retaining ledge should not break when doing this but it's 3d printed, don't do this too often.
* There is a hole ever so slightly too small for the power switch. Use a file to enlarge the hole until the switch can be press fit into the hole. Friction should be enough to keep it in place. If you file the hole too large, just use some more hot glue.

# Reference Documentation
* [Flora pinout diagram](https://learn.adafruit.com/getting-started-with-flora/flora-pinout-diagram)
* [PowerBoost charger pinout diagram](https://learn.adafruit.com/adafruit-powerboost-500-plus-charger/pinouts)
* [Inductive charger documentation](https://www.adafruit.com/product/1407)
