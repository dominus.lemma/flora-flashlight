#include <Adafruit_NeoPixel.h>
#include <Rotary.h>

#define ROTARY_A 2
#define ROTARY_B 3
#define PIN_BRIGHTNESS 0

//Pin8 is connected to the onboard NeoPixel
#define PIN 8
//In general, use NEO_GRB+NEO_KHZ800, other options are for older NeoPixels
Adafruit_NeoPixel pixel = Adafruit_NeoPixel(1, PIN, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel ring = Adafruit_NeoPixel(4, 6, NEO_GRB + NEO_KHZ800);
Rotary rotary = Rotary(ROTARY_A, ROTARY_B);

uint8_t colorIndex = 0;
uint8_t colorBrightness = 1;
uint8_t brightnessLastState = HIGH;

uint32_t colorsLow[] = {
  pixel.Color(64, 0, 0),
  pixel.Color(64, 32, 0),
  pixel.Color(64, 64, 0),
  pixel.Color(32, 64, 0),
  pixel.Color(0, 64, 0),
  pixel.Color(0, 64, 32),
  pixel.Color(0, 64, 64),
  pixel.Color(32, 64, 64),
  pixel.Color(64, 64, 64),
  pixel.Color(0, 32, 64),
  pixel.Color(0, 0, 64),
  pixel.Color(32, 0, 64),
  pixel.Color(64, 0, 64),
  pixel.Color(64, 0, 32)
};
uint32_t colorsMed[] = {
  pixel.Color(128, 0, 0),
  pixel.Color(128, 64, 0),
  pixel.Color(128, 128, 0),
  pixel.Color(64, 128, 0),
  pixel.Color(0, 128, 0),
  pixel.Color(0, 128, 64),
  pixel.Color(0, 128, 128),
  pixel.Color(64, 128, 128),
  pixel.Color(128, 128, 128),
  pixel.Color(0, 64, 128),
  pixel.Color(0, 0, 128),
  pixel.Color(64, 0, 128),
  pixel.Color(128, 0, 128),
  pixel.Color(128, 0, 64)
};
uint32_t colorsHigh[] = {
  pixel.Color(255, 0, 0),
  pixel.Color(255, 128, 0),
  pixel.Color(255, 255, 0),
  pixel.Color(128, 255, 0),
  pixel.Color(0, 255, 0),
  pixel.Color(0, 255, 128),
  pixel.Color(0, 255, 255),
  pixel.Color(128, 255, 255),
  pixel.Color(255, 255, 255),
  pixel.Color(0, 128, 255),
  pixel.Color(0, 0, 255),
  pixel.Color(128, 0, 255),
  pixel.Color(255, 0, 255),
  pixel.Color(255, 0, 128)
};

uint8_t colorsLength = sizeof(colorsHigh) / sizeof(colorsHigh[0]);
uint8_t lastA = 0;

void setup() {
  //configure rotary encoder pins
  pinMode(ROTARY_A, INPUT_PULLUP);
  pinMode(ROTARY_B, INPUT_PULLUP);
  pinMode(PIN_BRIGHTNESS, INPUT_PULLUP);

  //configure neopixels
  pixel.begin();
  pixel.setBrightness(255);

  ring.begin();
  ring.setBrightness(255);

  //set initial color
  showLights(pixel.Color(0, 0, 0));
}

void showLights(uint32_t color) {
  pixel.setPixelColor(0, color);
  pixel.show();

  ring.setPixelColor(0, color);
  ring.setPixelColor(1, color);
  ring.setPixelColor(2, color);
  ring.setPixelColor(3, color);
  ring.show();
}

void loop() {
  uint8_t brightnessState = digitalRead(PIN_BRIGHTNESS);
  if (brightnessState != brightnessLastState) {
    if (brightnessState == LOW) {
      colorBrightness = (colorBrightness + 1) % 3;
      brightnessLastState = brightnessState;
    }

    //debounce switch
    delay(20);
  }
  brightnessLastState = brightnessState;

  unsigned char result = rotary.process();
  if (result == DIR_CW) {
    colorIndex++;
  }
  else if (result == DIR_CCW) {
    colorIndex--;
  }

  //it's unsigned so wrapping around doesn't go negative, it goes maximum
  if (colorIndex == 255) {
    colorIndex = colorsLength - 1;
  }

  if (colorIndex == colorsLength) {
    colorIndex = 0;
  }

  switch (colorBrightness) {
    case 2:
      showLights(colorsHigh[colorIndex]);
      break;
    case 1:
      showLights(colorsMed[colorIndex]);
      break;
    default:
      showLights(colorsLow[colorIndex]);
  }
}
